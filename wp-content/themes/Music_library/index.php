<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <title>music app</title>
</head>
<body>
    <header>
        <div class="menu_side">
            <h1><i class="bi bi-music-player"></i>SongBox</h1>
             <div class="Songbox">
                <h6> <span></span>DISCOVER</h6>
                <h4> <span></span><i class="bi bi-house-door-fill"></i> Home</h4>
                <h4> <span></span><i class="bi bi-mic-fill"></i>Artist</h4>
                <h4> <span></span><i class="bi bi-music-note-beamed"></i>Albums</h4>
                <h4> <span></span><i class="bi bi-broadcast"></i>Radio</h4>
                <h4> <span></span><i class="bi bi-file-earmark-music-fill"></i>Playlist</h4>
                <h6> <span></span>LIBRARY</h6>
                <h4> <span></span><i class="bi bi-clock-fill"></i>Recent</h4>
                <h4> <span></span><i class="bi bi-music-note-beamed"></i>Albums</h4>
                <h4> <span></span><i class="bi bi-heart-fill"></i>Favorites</h4>
                <h6> <span></span>PLAYLIST</h6>
                <h4> <span></span><i class="bi bi-plus-square-fill"></i>Create Playlsit</h4>
                <h4> <span></span><i class="bi bi-file-earmark-music-fill"></i>Lonely Everyday</h4>
                <h4> <span></span><i class="bi bi-file-earmark-music-fill"></i>Lost in the dark</h4>
             </div>        
        </div> 
        <div class="song_side">
            <nav>
                <ul>
                   <li>MUSIC <span></span></li>
                   <li>PODCAST</li>
                   <li>OFFLINE</li>

                </ul>
                   
            </nav>
              </div>
             

    </header>

 <script src="app.js"> </script>

</body>
</html>